﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Persistence;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using Yoyocms.Umbraco7.TagManager.Models;

namespace Yoyocms.Umbraco7.TagManager
{
    [PluginController("TagManager")]
    public class TagManagerAPIController : UmbracoAuthorizedJsonController
    {
        public cmsTags GetTagById(int tagId)
        {
            cmsTags tag = new cmsTags();

            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var query = new Sql().Select(string.Format("id, tag, [group], propertytypeid, count(tagId) as noTaggedNodes FROM cmsTags LEFT JOIN cmsTagRelationship ON cmsTags.id = cmsTagRelationship.tagId Where cmsTags.Id = {0} GROUP BY tag, id, [group], propertytypeid;", tagId));

                tag = db.Fetch<cmsTags>(query).FirstOrDefault();

                List<TaggedDocument> taggedDocs = GetTaggedDocumentNodeIds(tagId);

                tag.taggedDocuments = taggedDocs;

                List<TaggedMedia> taggedMedia = GetTaggedMediaNodeIds(tagId);

                tag.taggedMedia = taggedMedia;

                TagInGroup tagsInGroup = GetAllTagsInGroup(tagId);

                tag.tagsInGroup = tagsInGroup;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in GetTagById:", ex);
            }


            return tag;
        }

        public IEnumerable<TagGroup> GetTagGroups()
        {
            IEnumerable<TagGroup> tagGroups = null;
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var query = new Sql().Select("[group] from cmstags GROUP BY [group] ORDER BY [group];");
                tagGroups = db.Fetch<TagGroup>(query);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in GetTagGroups:", ex);
            }

            return tagGroups;
        }

        public TagInGroup GetAllTagsInGroup(int tagId)
        {
            TagInGroup tagsInGroup = new TagInGroup();

            try
            {
                List<plainPair> listOfTags = new List<plainPair>();

                var db = ApplicationContext.DatabaseContext.Database;

                var groupNameQuery = new Sql().Select(string.Format("[group] FROM cmsTags WHERE id={0}", tagId));
                var resultGroupName = db.Single<string>(groupNameQuery);

                var query = new Sql().Select(string.Format("id, tag FROM cmsTags where [group] = '{0}' ORDER BY tag", resultGroupName));

                var results = db.Fetch<plainPair>(query);

                foreach (var result in results)
                {
                    plainPair t = new plainPair();
                    t.id = Convert.ToInt32(result.id);
                    t.tag = result.tag;

                    listOfTags.Add(t);

                    if (result.id == tagId)
                    {
                        tagsInGroup.selectedItem = t;
                    }
                }

                tagsInGroup.options = listOfTags;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in GetAllTagsInGroup:", ex);
            }

            return tagsInGroup;
        }

        public IEnumerable<cmsTags> GetAllTagsInGroup(string groupName)
        {
            IEnumerable<cmsTags> tags = null;
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var query = new Sql().Select(string.Format("id, tag, [group], count(tagId) as noTaggedNodes FROM cmstags LEFT JOIN cmsTagRelationship ON cmsTags.id = cmsTagRelationship.tagId WHERE [group] = '{0}' GROUP BY tag, id, [group] ORDER BY tag", groupName));

                tags = db.Fetch<cmsTags>(query);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in IEnumerable-cmsTags-GetAllTagsInGroup:", ex);
            }
            return tags;
        }

        public List<TaggedDocument> GetTaggedDocumentNodeIds(int tagId)
        {
            List<TaggedDocument> docs = new List<TaggedDocument>();

            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var query = new Sql().Select("nodeId as DocumentId").From("cmsTagRelationship").Where(string.Format("tagID = {0}", tagId));
                var results = db.Fetch<TaggedDocument>(query);
                foreach (var result in results)
                {

                    var n = Umbraco.Content(result.DocumentId);
                    if (!string.IsNullOrWhiteSpace(n.Name))
                    {
                        var document = new TaggedDocument { DocumentId = result.DocumentId, DocumentName = n.Name, DocumentURL = string.Format("/umbraco/#/content/content/edit/{0}", result.DocumentId.ToString()) };
                        docs.Add(document);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in GetTaggedDocumentNodeIds:", ex);
            }
            return docs;
        }

        public List<TaggedMedia> GetTaggedMediaNodeIds(int tagId)
        {

            List<TaggedMedia> meds = new List<TaggedMedia>();

            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var query = new Sql().Select("nodeId as DocumentId").From("cmsTagRelationship").Where(string.Format("tagID = {0}", tagId));
                var results = db.Fetch<TaggedDocument>(query);
                foreach (var result in results)
                {
                    var n = Umbraco.Media(result.DocumentId);
                    if (!string.IsNullOrWhiteSpace(n.Name))
                    {
                        var media = new TaggedMedia { DocumentId = result.DocumentId, DocumentName = n.Text, DocumentURL = string.Format("/umbraco/#/media/media/edit/{0}", result.DocumentId.ToString()) };
                        meds.Add(media);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in GetTaggedMediaNodeIds:", ex);
            }

            return meds;
        }

        public int MoveTaggedNodes(int currentTagId, int newTagId)
        {
            int success = 0;

            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                success = db.Execute("Update cmsTagRelationship SET tagID = @1 WHERE tagID = @0 AND nodeId NOT IN (SELECT nodeId FROM cmsTagRelationship WHERE tagId = @1);", currentTagId, newTagId);

                if (success == 1)
                {
                    success = db.Execute("DELETE FROM cmsTagRelationship WHERE tagId = @0;", currentTagId);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in MoveTaggedNodes:", ex);
            }
            return success;
        }

        public int Save(cmsTags tag)
        {
            int success = 0;

            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                success = db.Execute("Update cmsTags set tag = @0 where id = @1", tag.tag, tag.id);

                if (success == 1 && tag.id != tag.tagsInGroup.selectedItem.id)
                {
                    // Merge tags
                    string sqlQuery1 = string.Format("Update cmsTagRelationship SET tagID = {0} WHERE tagID = {1} AND nodeId NOT IN (SELECT nodeId FROM cmsTagRelationship WHERE tagId = {0});", tag.tagsInGroup.selectedItem.id, tag.id);

                    success = db.Execute(sqlQuery1);

                    // Delete tag
                    string sqlQuery2 = string.Format("DELETE FROM cmsTagRelationship WHERE tagId = {0};", tag.id);
                    db.Execute(sqlQuery2);
                }

                UpdateDocuments(tag);
                UpdateMedia(tag);

            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in Save:", ex);
            }

            return success;
        }

        private void UpdateDocuments(cmsTags tag)
        {
            try
            {
                if (tag.taggedDocuments.Count > 0)
                {
                    var contentService = ApplicationContext.Current.Services.ContentService;
                    var tagService = ApplicationContext.Current.Services.TagService;

                    foreach (TaggedDocument doc in tag.taggedDocuments)
                    {
                        var content = contentService.GetById(doc.DocumentId);
                        string propertyAlias = content.Properties.Where(x => x.PropertyType.Id == tag.propertytypeid).FirstOrDefault().Alias;
                        var tags = tagService.GetTagsForEntity(doc.DocumentId, tag.group);
                        IEnumerable<string> tagList = tags.Select(x => x.Text).ToList();
                        content.SetTags(propertyAlias, tagList, true, tag.group);
                        //publish change if no outstanding changes
                        if (content.Published)
                        {
                            contentService.SaveAndPublishWithStatus(content);
                        }
                        //save change only as other outstanding changes on node
                        else
                        {
                            contentService.Save(content);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in UpdateDocuments:", ex);
            }
        }

        private void UpdateMedia(cmsTags tag)
        {
            try
            {
                if (tag.taggedMedia.Count > 0)
                {

                    var mediaService = ApplicationContext.Current.Services.MediaService;
                    var tagService = ApplicationContext.Current.Services.TagService;

                    foreach (TaggedMedia med in tag.taggedMedia)
                    {
                        var content = mediaService.GetById(med.DocumentId);
                        string propertyAlias = content.Properties.Where(x => x.PropertyType.Id == tag.propertytypeid).FirstOrDefault().Alias;
                        var tags = tagService.GetTagsForEntity(med.DocumentId, tag.group);
                        IEnumerable<string> tagList = tags.Select(x => x.Text).ToList();
                        content.SetTags(propertyAlias, tagList, true, tag.group);
                        mediaService.Save(content);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(TagManagerAPIController), "Error in UpdateMedia:", ex);
            }
        }

        [System.Web.Http.HttpPost]
        [AcceptVerbs("POST", "GET")]
        public int DeleteTag(cmsTags tag)
        {
            var db = ApplicationContext.DatabaseContext.Database;
            string sqlQuery1 = string.Format("DELETE FROM cmsTagRelationship WHERE tagId = {0};", tag.id);
            db.Execute(sqlQuery1);

            string sqlQuery2 = string.Format("DELETE FROM cmsTags WHERE id = {0};", tag.id);
            int success = db.Execute(sqlQuery2);

            UpdateDocuments(tag);
            UpdateMedia(tag);

            return success;
        }

        //[Obsolete]
        //public int AddDbEntry(string sqlStatement)
        //{
        //    var db = ApplicationContext.DatabaseContext.Database;
        //    var query = new Sql(sqlStatement);
        //    return db.Execute(query);
        //}

        //[Obsolete]
        //public int checkUserTable()
        //{
        //    var db = ApplicationContext.DatabaseContext.Database;
        //    string sql = "SELECT * FROM umbracoUser2App WHERE [user] = 0 and [app] = 'TagManager';";
        //    int success = db.Execute(sql);
        //    return success;
        //}

    }
}