﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.IO;
using Umbraco.Core.Models;

namespace Yoyocms.Umbraco7.TagManager
{
    public class TagManagerStartup : ApplicationEventHandler
    {
        protected const string TagManagerSectionAlias = "TagManager";

        protected override void ApplicationStarted(UmbracoApplicationBase umbraco, ApplicationContext context)
        {
            // Gets a reference to the section (if already added)
            //Section section = context.Services.SectionService.GetByAlias(TagManagerSectionAlias);
            //if (section != null) return;

            // Add a new "Skrift Demo" section
            //context.Services.SectionService.MakeNew("Tag Manager", TagManagerSectionAlias, "icon-tags", 15);

            // Grant all existing user groups access to the new section
            foreach (var group in ApplicationContext.Current.Services.UserService.GetAllUserGroups())
            {
                 group.AddAllowedSection(TagManagerSectionAlias);
            }
        }
    }
}
